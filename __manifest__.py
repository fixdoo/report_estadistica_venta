{
    'name': 'Reporte estadisticas de venta',
    'version': '14.0.1.0.0',
    'category': 'sale',
    'summery': 'Reporte estadisticas de venta',
    'author': 'Fixdoo',
    'depends': ['stock', 'sale_management'],
    'data': [
        'security/ir.model.access.csv',
        'wizard/wizard_order_timeline_report.xml',
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': False,
}
