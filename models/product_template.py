from odoo import api, models, fields


class ProductTemplate(models.Model):
    _name = "product.template"

    standard_price = fields.Float(store=True)
