from dateutil.relativedelta import relativedelta

from odoo import fields, models, tools, _


class WizardReportUnidadProducto(models.TransientModel):
    _name = "wizard.report.unidad.producto"

    def _get_default_date_init(self):
        datetime_from = fields.Datetime.now() - relativedelta(days=1)
        return datetime_from

    def _get_default_date_end(self):
        datetime_to = fields.Datetime.now()
        return datetime_to

    date_init = fields.Datetime(string="Fecha Inicial" )
    date_end = fields.Datetime(string="Fecha Final" )
    partner_id = fields.Many2one('res.partner', string="Cliente")

    def get_report(self):
        report_producto = self.env["report.unidad.producto"]
        reports_ids = report_producto.search([])
        reports_ids.unlink()
        self._cr.execute(self._get_main_request(self.date_init, self.date_end, self.partner_id.id))
        for rec in self._cr.dictfetchall():
            record = self.env['order.timeline.report'].sudo().create({
                'product_id': rec['product_id'],
                'product_id_devolucion': rec['product_id_devolucion'],
                'currency_id': rec['currency_id'],
                'currency_id_devolucion': rec['currency_id_devolucion'],
                'monto_facturado': rec['monto_facturado'],
                'monto_devuelto': rec['monto_devuelto'],
            })
        action = self.env["ir.actions.actions"]._for_xml_id("report_estadistica_venta.action_report_unidad_producto")
        action["domain"] = []
        context = dict(date_init=self.date_init, date_end=self.date_end, partner_id=self.partner_id)
        action['context'] = context
        return action

    def _get_main_request(self, date_init, date_end, partner_id):
        request = """
                WITH devoluciones AS (
                    SELECT 
                        sm.product_id AS product_id_devolucion,
                        sol.currency_id  AS currency_id_devolucion,
                        sum(sol.price_total) AS monto_devuelto
                    FROM 
                        stock_move AS sm
                    INNER JOIN 
                        sale_order_line AS sol
                    ON
                        sol.id = sm.sale_line_id
                    WHERE sm.to_refund = True and sm.partner_id = '%s' and sm.date >= '%s' and sm.date <= '%s'
                    GROUP BY
                        sm.product_id,
                        sol.currency_id
                ),facturado AS (
                    SELECT
                        sm.product_id AS product_id,
                        sol.currency_id  AS currency_id,
                        sum(sol.price_total) AS monto_facturado
                    FROM 
                        stock_move AS sm
                    INNER JOIN 
                        sale_order_line AS sol
                    ON
                        sol.id = sm.sale_line_id
                    WHERE sm.to_refund = False and sm.partner_id = '%s' and sm.date >= '%s' and sm.date <= '%s'
                    GROUP BY
                        sm.product_id,
                        sol.currency_id
                )
                SELECT 
                    *
                FROM facturado FULL OUTER JOIN devoluciones ON facturado.product_id = devoluciones.product_id_devolucion
        """ % (partner_id, date_init, date_end, partner_id, date_init, date_end)
        return request


class ReportUnidadProducto(models.Model):
    _name = "report.unidad.producto"

    referencia_interna = fields.Char(string="Referencia Interna")
    product_id = fields.Many2one('product.product', string="Nombre")
    product_id_devolucion = fields.Many2one('product.product', string="Producto devolución")
    currency_id = fields.Many2one('res.currency', sstring="Moneda")
    currency_id_devolucion = fields.Many2one('res.currency', string="Moneda devolucion")
    monto_facturado = fields.Float(string="Monto Facturado")
    monto_devuelto = fields.Float(string="Monto Devuelto")
